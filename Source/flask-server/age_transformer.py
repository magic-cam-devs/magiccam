import io
import PIL.Image
import requests

from typing import List

from transformation import ImageTransformer


class AgeTransformer(ImageTransformer):

    def __init__(self):
        super().__init__()
        self._image_width = 200
        self._image_height = 200
        self._tf_serving_host = '192.168.1.47'
        self._tf_serving_port = 5000

    def transform(self,
                  image_bytes: bytes,
                  selected_attrs: List[str]) -> bytes:
        src_image = PIL.Image.open(io.BytesIO(image_bytes))
        src_image_size = src_image.size

        src_image = src_image.resize((self._image_width, self._image_height))
        bytesio = io.BytesIO()
        src_image.save(bytesio, format='jpeg')
        src_image.close()

        image_bytes = bytesio.getvalue()
        response = requests.post('http://{}:{}'
                                 .format(self._tf_serving_host,
                                         self._tf_serving_port),
                                 data=image_bytes)

        output_bytes = response.content
        output_image = PIL.Image.open(io.BytesIO(output_bytes))
        output_image = output_image.resize(src_image_size)
        bytesio = io.BytesIO()
        output_image.save(bytesio, format='jpeg')
        output_image.close()
        output_bytes = bytesio.getvalue()
        return output_bytes
